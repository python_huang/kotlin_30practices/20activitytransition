package com.example.a20activitytransition

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.Explode
import android.transition.Slide
import android.transition.TransitionInflater

class Activity_2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)

        setEnterAnimation()
    }

    private fun setEnterAnimation() {
        when (intent.getStringExtra("Transition")) {
            "android.transition.Explode" -> {
                val explode = Explode()
                explode.duration = 1000
                window.enterTransition = explode
            }

            "android.transition.Slide" -> {
                val slide = Slide()
                slide.duration = 1000
                window.enterTransition = slide
            }

            "android.transition.Fade" -> {
                val fade =
                    TransitionInflater.from(this).inflateTransition(R.transition.activity_fade)
                window.enterTransition = fade
            }
        }
    }

}